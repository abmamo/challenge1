# idenfity node with most connections


class Node:
    def __init__(self, label):
        # unique node idenfier
        # Each node is unique thus there will be no cases of having multiple nodes with the same label
        self.label = label
        # define infinite number of connection to and from node using lists
        # Each node will have an infinite number of both inbound and outbound links.
        self.nodes_from = []
        self.nodes_to = []

    def get_label(self):
        return self.label

    def add_from(self, node_label):
        # add connection to node
        self.nodes_from.append(node_label)

    def add_to(self, node_label):
        # add connections from node
        self.nodes_to.append(node_label)

    def get_num_connections(self):
        return len(self.nodes_from) + len(self.nodes_to)


class Graph:
    def __init__(self):
        # initialize empty dict to save nodes and their label indentifiers
        self.nodes = {}

    def add_node(self, node_label):
        # add node label name and node to nodes dictionary
        self.nodes[node_label] = Node(node_label)

    def delete_node(self, node_label):
        # remode node from dictionary / pop also returns the removed value
        self.nodes.pop(node_label)

    def get_node(self, node_label):
        # get node using node label
        return self.nodes[node_label]

    def add_edge(self, from_node_label, to_node_label):
        # get both nodes using their labels
        from_node, to_node = self.get_node(
            from_node_label), self.get_node(to_node_label)
        # update both to and from connections
        from_node.add_to(from_node_label)
        to_node.add_from(to_node_label)


def get_num_connections(directed_graph):
    """
        get a directed graph and return a dict with node
        labels as keys and their number of connections as values
    """
    # get nodes
    nodes = directed_graph.nodes
    # define dict to save number of connections in
    num_connections = {}
    # get num connections for each node
    for node_label in nodes:
        # get numer connections
        node_connections = nodes[node_label].get_num_connections()
        # save in dictionary
        num_connections[node_label] = node_connections
    return num_connections


def identify_router(directed_graph):
    """
        get a directed graph and return a list with the nodes
        with the maximum number of connections. if a graph
        has two connections with equal maximum number of
        connections return a list of both
    """
    # get number of connections for each node
    num_connections = get_num_connections(directed_graph)
    # return values with the max value in dictionary
    return [node_label for node_label in num_connections.keys() if num_connections[node_label] == max(num_connections.values())]


# test case one
# define graph
g = Graph()
# add nodes
g.add_node('1')
g.add_node('2')
g.add_node('3')
g.add_node('5')
# add connections
g.add_edge('1', '2')
g.add_edge('2', '3')
g.add_edge('3', '5')
g.add_edge('5', '2')
g.add_edge('2', '1')
# get router node
most_connections_node = identify_router(g)
# test
assert most_connections_node == ['2']
# test case two
# define graph
g = Graph()
# add nodes
g.add_node('1')
g.add_node('2')
g.add_node('3')
g.add_node('4')
g.add_node('5')
g.add_node('6')
# add connections
g.add_edge('1', '3')
g.add_edge('3', '5')
g.add_edge('5', '6')
g.add_edge('6', '4')
g.add_edge('4', '5')
g.add_edge('5', '2')
g.add_edge('2', '6')
# get router node
most_connections_node = identify_router(g)
# test
assert most_connections_node == ['5']
# test case three
# define graph
g = Graph()
# add nodes
g.add_node('2')
g.add_node('4')
g.add_node('5')
g.add_node('6')
# add connections
g.add_edge('2', '4')
g.add_edge('4', '6')
g.add_edge('6', '2')
g.add_edge('2', '5')
g.add_edge('5', '6')
# get router node
most_connections_node = identify_router(g)
assert most_connections_node == ['2', '6']


'''
Explanation
___________

For a given graph G with number of nodes n

The identify_router function gets the number of
connections for each node in a directed graph using
the helper function get_num_connections. Since it 
iterates through g, it has a runtime of O(n)

The get_num_connections function in turn iterates 
through each node in the directed graph also is dependent
on the get_num_connections method of the Node class 
which uses the built in python list len function.
Since it also iterates through g, it has a runtime of O(n)

The len function for lists (and other built in iterables) 
runs O(1).

Therefore since we iterate over g twice within the same function
(non nested) the runtime would be O(2n).

'''
