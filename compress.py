'''
    String Compression
    Idea:   use a dictionary to keep count of currently seen character and when a new
            character is encountered reset the counter dictionary
'''


def occurence_to_string(occurence_dict):
    '''
        helper function to convert dictionary of occurence
        of the form {a: 3, b: 5} to 'a3b5' for instance
    '''
    # define emtpy string
    string = ""
    # iterate through dictionary
    for key in occurence_dict:
        # append key value pair to string
        string += str(key) + str(occurence_dict[key])
    # return dict converted to string
    return string


def compress(string):
    # resulting compressed string
    compressed_string = ""
    # create a dictionary to keep count of occurence
    occurence = {}
    # iterate through each char
    for char in string:
        # if there is no occurence count of the current counter
        if not occurence:
            # set count to 1
            occurence[char] = 1
        # if there is an occurence count for current char
        else:
            # if current character is in the occurence dictionary
            if char in occurence:
                # update count
                occurence[char] += 1
            # if current character is not in occurence dictionary
            elif char not in occurence:
                # save occurence count to string
                compressed_string += occurence_to_string(occurence)
                # reset occurence
                occurence = {}
                # set for current character
                occurence[char] = 1
    # update with occurence of last char sequence
    compressed_string += occurence_to_string(occurence)
    # return string
    return compressed_string


assert compress("bbcceeee") == "b2c2e4"
assert compress("aaabbbcccaaa") == "a3b3c3a3"
assert compress("a") == "a1"


'''
### Explanation of time complexity
Since the runtime of the conditional statements is the same
everytime we run them we can say they have a constant complexity O(1) time. 

The only two places where we encounter for loops are in the
compress function and in the helper function.

In the compress function the runtime is linear O(n) with respect
to n where n is the length of the input string because it iterates
n times for a string of length n.

In the helper function the runtime is O(1) with respect
to n where n is the length of the input string because
it just takes a dictionary of the from {a: 3} and returns
a3. The runtime of this function is not dependent on n
the length of the input string.

'''
